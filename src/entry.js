import Vue from 'vue';

// import Vuetify, {
//   VApp, // required
//   VTextField,
//   VCombobox,
//   VFlex,
//   VLayout
// } from 'vuetify/lib'


import test from './store/modules/test/store.js';

import Test from './components/Test.vue';



const components = { Test }
const modules = { test }
// const vuetifyComponents = {
//   VApp, // required
//   VTextField,
//   VCombobox,
//   VFlex,
//   VLayout
// }

function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  // Object.keys(vuetifyComponents).forEach(name => {
  //   Vue.component(name, vuetifyComponents[name])
  // });

  Object.keys(components).forEach(name => {
    Vue.component(name, components[name])
  });

}

const plugin = {
  install,
}

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}


export default components
export { Test } // make each component individually exposed
export { test } // make each module individually exposed
