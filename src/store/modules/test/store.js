
export default {
  namespaced: true,
  state: () => ({
    isATest: true
  }),
  mutations: {
    set(state, {k, v}) { state[k] = v },

  },
  actions: {

  }
}
