# vue-vuex-sfc-lib

## How it was made

```
vue create vue-vuex-sfc-lib # (vuex added)
vue add vuetify # (default)
```

updated the `package.json`

added `rollup.config.js`


## Build library via vue-cli
```
npm run build:lib
```
(doesn't let me import)

## Build library via rollup
```
npm run r:build
```
(lets me import, but issue with vuetify registration)


see:

[![Edit Vue Vuex SFC Lib Demo](https://codesandbox.io/static/img/play-codesandbox.svg)](https://codesandbox.io/s/qvz4jy9429)


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
